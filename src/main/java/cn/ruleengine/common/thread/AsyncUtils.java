package cn.ruleengine.common.thread;

import cn.ruleengine.common.collection.CollUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author dingqianwen
 * @date 2021/1/29
 * @since 1.0.0
 */
public class AsyncUtils {


    public static <T, OUT> List<OUT> batch(ExecutorService executorService, List<T> list, int aFew, Batch<T, OUT> batchExecutor) {
        List<List<T>> lists = CollUtils.subList(list, aFew);
        if (lists.isEmpty()) {
            return Collections.emptyList();
        }
        return AsyncUtils.batch(executorService, lists, batchExecutor);
    }

    public static <T, OUT> List<OUT> batch(ExecutorService executorService, List<List<T>> lists, Batch<T, OUT> batchExecutor) {
        List<Future<List<OUT>>> futures = new ArrayList<>();
        for (List<T> list : lists) {
            Future<List<OUT>> future = executorService.submit(() -> {
                List<OUT> outs = new ArrayList<>(list.size());
                for (T in : list) {
                    OUT out = null;
                    try {
                        out = batchExecutor.async(in);
                    } catch (Exception e) {
                        e.printStackTrace();
                        batchExecutor.onError(in, e);
                    }
                    outs.add(out);
                }
                return outs;
            });
            futures.add(future);
        }
        List<OUT> values = null;
        // 等待线程执行完毕
        for (Future<List<OUT>> future : futures) {
            try {
                List<OUT> value = future.get();
                if (values == null) {
                    values = value;
                } else {
                    values.addAll(value);
                }
            } catch (Exception e) {
                e.printStackTrace();
                // 结束未完成的线程
                Stream.of(future).filter(f -> !f.isDone()).forEach(m -> {
                    m.cancel(true);
                });
                exceptionHandle(e);
            }
        }
        return values;
    }


    @SafeVarargs
    public static <OUT> List<OUT> merge(ExecutorService executorService, boolean ignoreException, Merge<OUT>... merges) {
        List<Future<OUT>> futures = new ArrayList<>();
        for (Merge<OUT> merge : merges) {
            Future<OUT> future = executorService.submit(() -> {
                OUT out = null;
                try {
                    out = merge.async();
                } catch (Exception e) {
                    if (ignoreException) {
                        e.printStackTrace();
                    } else {
                        throw e;
                    }
                }
                return out;
            });
            futures.add(future);
        }
        List<OUT> values = new ArrayList<>();
        for (Future<OUT> future : futures) {
            try {
                OUT value = future.get();
                values.add(value);
            } catch (Exception e) {
                // 结束未完成的线程
                Stream.of(future).filter(f -> !f.isDone()).forEach(m -> {
                    m.cancel(true);
                });
                exceptionHandle(e);
            }
        }
        return values;
    }


    /**
     * 注意：不会保证执行调用顺序
     *
     * @param executorService 线程池  例：{ExecutorService executorService = Executors.newFixedThreadPool(10);}
     * @param ignoreException 是否忽略异常信息 如果为false 在程序执行时出现异常则终止此方法，如果为true 则只出现异常的Concurrent方法被终止
     * @param collects        collects
     */
    public static <R> void collect(ExecutorService executorService, boolean ignoreException, Collect... collects) {
        List<Future<R>> futures = new ArrayList<>();
        for (Collect collect : collects) {
            Future<R> future = executorService.submit(() -> {
                try {
                    collect.async();
                } catch (Exception e) {
                    if (ignoreException) {
                        e.printStackTrace();
                    } else {
                        throw e;
                    }
                }
                return null;
            });
            futures.add(future);
        }
        for (Future<R> future : futures) {
            try {
                // 超时时间  默认10s
                future.get(10, TimeUnit.SECONDS);
            } catch (Exception e) {
                // 结束未完成的线程
                Stream.of(future).filter(f -> !f.isDone()).forEach(m -> {
                    m.cancel(true);
                });
                exceptionHandle(e);
            }
        }
    }

    private static void exceptionHandle(Exception e) {
        if (e.getCause() != null) {
            if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException) e.getCause();
            }
            throw new RuntimeException(e.getCause());
        }
        throw new RuntimeException(e);
    }

}
