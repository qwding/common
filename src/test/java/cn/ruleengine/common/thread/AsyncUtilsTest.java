package cn.ruleengine.common.thread;


import lombok.Data;
import org.junit.After;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author dingqianwen
 * @date 2021/1/29
 * @since 1.0.0
 */
public class AsyncUtilsTest {

    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    @Data
    public static class TerminalVo {
        private String id;

        private String sn;

        private String factoryName;

        private String modelName;
    }

    @Test
    public void mergeTest() {
        List<String> merge = AsyncUtils.merge(executorService, true,
                new Merge<String>() {
                    @Override
                    public String async() throws Exception {
                        Thread.sleep(1000);
                        return "1";
                    }
                }, new Merge<String>() {
                    @Override
                    public String async() throws Exception {
                        Thread.sleep(1000);
                        return "2";
                    }
                }, new Merge<String>() {
                    @Override
                    public String async() throws Exception {
                        Thread.sleep(1000);
                        return "3";
                    }
                }, new Merge<String>() {
                    @Override
                    public String async() throws Exception {
                        Thread.sleep(1000);
                        return "4";
                    }
                });
        System.out.println(merge);
    }

    public static String getFactoryName(String factoryId) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "厂商A";
    }

    public static String getModelName(String modelId) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "POS_A";
    }


    @Test
    public void collectTest() {
        long startTime = System.currentTimeMillis();

        TerminalVo terminalVo = new TerminalVo();
        terminalVo.setId("1");
        terminalVo.setSn("0000119212312389001");

        // 使用自定义线程池 AsyncHelper
        AsyncUtils.collect(this.executorService, true,
                () -> {
                    // feign interface 1
                    String factoryName = getFactoryName("1");
                    terminalVo.setFactoryName(factoryName);
                },
                () -> {
                    // feign interface 2
                    String modelName = getModelName("1");
                    terminalVo.setModelName(modelName);
                });

        System.out.println(terminalVo);

        // 1058
        System.out.println(System.currentTimeMillis() - startTime);
    }


    @Test
    public void batchTest() {
        long startTime = System.currentTimeMillis();
        List<String> datas = Arrays.asList("1", "2");
        List<String> batch = AsyncUtils.batch(executorService, datas, 2, new Batch<String, String>() {
            @Override
            public String async(String data) throws Exception {
                Thread.sleep(1000);
                return data + "1";
            }

            @Override
            public void onError(String data, Exception e) throws Exception {

            }
        });
        System.out.println(batch);
        // 1034
        System.out.println(System.currentTimeMillis() - startTime);
    }

    @After
    public void after() {
        executorService.shutdown();
    }

}
